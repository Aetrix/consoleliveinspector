﻿using System.Collections.Generic;
using static Console.DevConsole; 

namespace Console
{
    public class CommandHelp : ConsoleCommand
    {
        public sealed override string Name { get; protected set; }
        public sealed override string Command { get; protected set; }
        public sealed override string Description { get; protected set; }
        public sealed override string Help { get; protected set; }
        public sealed override string Example { get; protected set; }

        public CommandHelp()
        {
            Name = "Help";
            Command = "help";
            Description = "<color='#ffffff'>Returns description for specified command (or all available commands if parameter is not specified)</color>";
            Help = "<color='#ffffff'>Syntax: help <command name></color> \n" +
                   $"<color='#7BF566'>|- </color><color={OptionalColor}><command name></color><color='#ffffff'> is optional</color>";
            Example = "<color='#ffffff'>help set</color>";

            AddCommandToConsole();
        }

        public override void RunCommand(string[] data)
        {
            if (data.Length == 1)
            {
                //AddStaticMessageToConsole("____________________________________ ___ __ _");
                AddStaticMessageToConsole("<color='#ffffff'>Available commands</color>");

                int indexCounter = 1;

                foreach (KeyValuePair<string, ConsoleCommand> command in Commands)
                {
                    AddStaticMessageToConsole("<color='#ffffff'>" + indexCounter + ") " + command.Key + "</color>");

                    indexCounter++;
                }
                //AddStaticMessageToConsole("\n");
            }
            else if (data.Length == 2 && Commands.ContainsKey(data[1]))
            {
                var parameter = data[1];

                var command = Commands[parameter];

                //AddStaticMessageToConsole("____________________________________ ___ __ _");
                AddStaticMessageToConsole("<color='#ffffff'><b>Title of command</b></color>");
                AddStaticMessageToConsole("|- <color='#ffffff'>" + command.Name + "</color>");
                AddStaticMessageToConsole("<color='#ffffff'><b>Description</b></color>");
                AddStaticMessageToConsole("|- <color='#ffffff'>" + command.Description + "</color>");
                AddStaticMessageToConsole("<color='#ffffff'><b>Usage</b></color>");
                AddStaticMessageToConsole("|- <color='#ffffff'>" + command.Help + "</color>");
                AddStaticMessageToConsole("<color='#ffffff'><b>Example</b></color>");
                AddStaticMessageToConsole("|- <color='#ffffff'>" + command.Example + "</color>");
            }
            else if (Commands.ContainsKey(data[1]) == false)
            {
                AddStaticMessageToConsole(NotRecognized);
            }
            else
            {
                AddStaticMessageToConsole(ParametersAmount);
            }

        }

        public static CommandHelp CreateCommand()
        {
            return new CommandHelp();
        }
    }
}