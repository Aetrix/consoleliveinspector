﻿using System.Collections.Generic;
using static Console.DevConsole; 

namespace Console
{
    public class CommandClearConsole : ConsoleCommand
    {
        public sealed override string Name { get; protected set; }
        public sealed override string Command { get; protected set; }
        public sealed override string Description { get; protected set; }
        public sealed override string Help { get; protected set; }
        public sealed override string Example { get; protected set; }

        public CommandClearConsole()
        {
            Name = "Clear Console";
            Command = "clear";
            Description = "Clear console.";
            Help = "Syntax: help <command name> \n" +
                   $"<color={OptionalColor}><command name></color> is optional";
            Example = "clear";

            AddCommandToConsole();
        }

        public override void RunCommand(string[] data)
        {
            DevConsole.Instance._consoleText.text = ""; //data[0].ToString()
                                                        //AddStaticMessageToConsole(ParametersAmount);
        }

        public static CommandClearConsole CreateCommand()
        {
            return new CommandClearConsole();
        }
    }
}
