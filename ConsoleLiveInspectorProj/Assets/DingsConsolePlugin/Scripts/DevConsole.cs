﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Console
{
    public abstract class ConsoleCommand
    {
        public abstract string Name { get; protected set; }

        public abstract string Command { get; protected set; }

        public abstract string Description { get; protected set; }

        public abstract string Help { get; protected set; }

        public abstract string Example { get; protected set; }

        public void AddCommandToConsole()
        {

            DevConsole.AddCommandsToConsole(Command, this);

        }

        public abstract void RunCommand(string[] data);
    }

    public class DevConsole : MonoBehaviour
    {
        #region Properties
        public static DevConsole Instance { get; set; }

        //public bool ConsoleDebugLogsOnStartUp;

        public static Dictionary<string, ConsoleCommand> Commands { get; set; }

        [SerializeField]
        private Canvas _consoleCanvas;

        [SerializeField]
        private ScrollRect _scrollRect;

        [SerializeField]
        public Text _consoleText;

        [SerializeField]
        private Text _inputText;

        [SerializeField]
        private InputField _consoleInput;

        [SerializeField]
        public Dropdown dropDown;

        [SerializeField]
        public RectTransform _consoleRect;

        public List<string> dropDownCommands;
        #endregion

        #region Colors

        public static string RequiredColor = "#FA8072";

        public static string OptionalColor = "#00FF7F";

        public static string WarningColor = "#ffcc00";

        public static string ExecutedColor = "#e600e6";


        public static string LogColor = "#36C7FF";

        public static string WarningLogColor = "#FFF536";

        public static string ErrorLogColor = "#FF3677";

        public static string ExceptionLogColor = "#FF00B9";


        public static string LogIcon = "<color='#36C7FF'>| + |</color>";

        public static string WarningLogIcon = "<color='#FFF536'>{ ! }</color>";

        public static string ErrorLogIcon = "<color='#FF3677'>[ x ]</color>";

        public static string ExceptionLogIcon = "<color='#FF00B9'>[ !x! ]</color>";

        #endregion

        #region Typical Console Messages

        public static string NotRecognized = $"Command not <color={WarningColor}>recognized</color>!";

        public static string ExecutedSuccessfully = $"Command executed <color={ExecutedColor}>successfully</color>";

        public static string ParametersAmount = $"Wrong <color={WarningColor}>amount of parameters</color>";

        public static string TypeNotSupported = $"Type of command <color={WarningColor}>not supported</color>!";

        public static string SceneNotFound = $"Scene <color={WarningColor}>not found</color>!" +
                                             $" Make sure that you have placed it inside <color={WarningColor}>build settings</color>.";       

        #endregion

        private void Awake()
        {
            if (Instance != null)
            {
                return;
            }

            Instance = this;

            Commands = new Dictionary<string, ConsoleCommand>();

            //if (ConsoleDebugLogsOnStartUp)
            //{
            //    Application.logMessageReceived += HandleLog;
            //}
        }

        private void Start()
        {
            _consoleCanvas.gameObject.SetActive(false);

            _consoleText.text = "<color='white'><size='20'>Console Live Inspector V 1.0.0</size></color> \n";

            CreateCommands();
            PopulateDropdown();
        }

        private void CreateCommands()
        {
            CommandHelp.CreateCommand();

            CommandClearConsole.CreateCommand();

            CommandCoreConsole.CreateCommand();

            CommandSceneList.CreateCommand();

            CommandLoadScene.CreateCommand();

            CommandGetKeyValue.CreateCommand();
        }

        private void OnEnable()
        {
            Application.logMessageReceived += HandleLog;
        }

        private void OnDisable()
        {
            Application.logMessageReceived -= HandleLog;
        }

        public void HandleLog(string logMessage, string stackTrace, LogType type)
        {
            //Debug.Log(type.ToString());
            string _color = "";
            string _icon = "";
            if(type.ToString() == "Log")
            {
                _color = LogColor;
                _icon = LogIcon;
            }
            else if (type.ToString() == "Warning")
            {
                _color = WarningLogColor;
                _icon = WarningLogIcon;
            }
            else if (type.ToString() == "Error")
            {
                _color = ErrorLogColor;
                _icon = ErrorLogIcon;
            }
            else if (type.ToString() == "Exception")
            {
                _color = ExceptionLogColor;
                _icon = ExceptionLogIcon;
            }
            string _message = $"{_icon}<color={_color}> [ " + type.ToString() + " ]</color> " + "<color='#ffffff'>" + logMessage + "</color> \n" + "<color='#7BF566'>| \n" + "<size=11>" + stackTrace +"</size>" + "|</color>";
            AddMessageToConsole(_message);
        }

        public static void AddCommandsToConsole(string name, ConsoleCommand command)
        {
            if (!Commands.ContainsKey(name))
            {
                
                Commands.Add(name, command);
                
                Debug.Log("<color='#FCFF00'>[ " + name + " ]</color>" + " has been added to console.");
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.BackQuote))
            {
                _consoleCanvas.gameObject.SetActive
                    (!_consoleCanvas.gameObject.activeInHierarchy);

                _consoleInput.ActivateInputField();
                _consoleInput.Select();
            }

            if (_consoleCanvas.gameObject.activeInHierarchy)
            {
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    if (string.IsNullOrEmpty(_inputText.text) == false)
                    {
                        AddMessageToConsole(_inputText.text);

                        ParseInput(_inputText.text);
                    }

                    // Clears input
                    _consoleInput.text = "";

                    _consoleInput.ActivateInputField();
                    _consoleInput.Select();

                    dropDown.value = 0;
                }
            }

            if (_consoleCanvas.gameObject.activeInHierarchy == false)
            {
                _consoleInput.text = "";
            }

        }

        private IEnumerator ScrollDown()
        {
            yield return new WaitForSeconds(0.1f);
            _scrollRect.verticalNormalizedPosition = 0f;
        }

        private void AddMessageToConsole(string msg)
        {
            StringBuilder sb = new StringBuilder(_consoleText.text);
            
            sb.AppendLine("<color='#7BF566'>|- " + msg + "</color>");
            _consoleText.text = sb.ToString();

            StartCoroutine(ScrollDown());
        }

        public static void AddStaticMessageToConsole(string msg)
        {
            //Instance._consoleText.text += "<color='#7BF566'>|- </color>" + msg + "\n";

            StringBuilder sb = new StringBuilder(Instance._consoleText.text);

            sb.AppendLine("<color='#7BF566'>|- " + msg + "</color>");
            Instance._consoleText.text = sb.ToString();

            Instance.StartCoroutine(Instance.ScrollDown());
        }

        private void ParseInput(string input)
        {
            string[] commandSplitInput = input.Split(null);

            if (string.IsNullOrWhiteSpace(input))
            {
                AddMessageToConsole(NotRecognized);
                return;
            }

            if (Commands.ContainsKey(commandSplitInput[0]) == false)
            {
                AddMessageToConsole(NotRecognized);
            }
            else
            {
                Commands[commandSplitInput[0]].RunCommand(commandSplitInput);
            }

            // Force scroll
            StartCoroutine(ScrollDown());
        }

        public void MobileConsoleCall()
        {
            if (_consoleCanvas.gameObject.activeInHierarchy)
            {
                
                    if (string.IsNullOrEmpty(_inputText.text) == false)
                    {
                        AddMessageToConsole(_inputText.text);

                        ParseInput(_inputText.text);
                    }

                    // Clears input
                    _consoleInput.text = "";

                    //_consoleInput.ActivateInputField();
                    //_consoleInput.Select();
                    dropDown.value = 0;
                
            }

            if (_consoleCanvas.gameObject.activeInHierarchy == false)
            {
                _consoleInput.text = "";
            }
        }

        public void PopulateDropdown()
        {
            var something = new Dropdown.OptionData();
            dropDownCommands.Add("Enter Command...");
            something.text = "Enter Command...";
            dropDown.options.Add(something);
            foreach (var item in Commands.Values)
            {
                var something2 = new Dropdown.OptionData();
                something2.text = item.Command;
                dropDown.options.Add(something2);
                dropDownCommands.Add(item.Command);
            }

        }

        public void DropDownIndexChanged(int index)
        {

            if (index == 0)
            {
                DevConsole.Instance._consoleInput.text = "";
            }
            else
            {
                DevConsole.Instance._consoleInput.text = dropDownCommands[index];
            }
            
        }
    }
}