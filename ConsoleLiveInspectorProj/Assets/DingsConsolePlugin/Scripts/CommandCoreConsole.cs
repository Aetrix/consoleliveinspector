﻿using System.Collections;
using System.Collections.Generic;
using Console;
using UnityEngine;
using static Console.DevConsole;

public class CommandCoreConsole : ConsoleCommand
{
    public sealed override string Name { get; protected set; }
    public sealed override string Command { get; protected set; }
    public sealed override string Description { get; protected set; }
    public sealed override string Help { get; protected set; }
    public sealed override string Example { get; protected set; }

    public CommandCoreConsole()
    {
        Name = "Console Screen Mode";
        Command = "console";
        Description = "Set console position properties";
        Help = "Syntax: console <mode> <option>, console <debug> <option> \n" +
               $"<color='#7BF566'>|- </color><color=>All parameters are required!</color>";
        Example = "console mode full, console mode half, console mode small, console mode lite, console mode minimal\n<color='#7BF566'>|- </color>console debug true, console debug false, console debug resolution";

        AddCommandToConsole();
    }

    public override void RunCommand(string[] data)
    {
        if (data.Length == 3)
        {
            var mode = data[1].ToLower();
            var option = data[2].ToLower();

            if(mode == "mode")
            {


                ///*Left*/
                //rectTransform.offsetMin.x;
                ///*Right*/
                //rectTransform.offsetMax.x;
                ///*Top*/
                //rectTransform.offsetMax.y;
                ///*Bottom*/
                //rectTransform.offsetMin.y;
                if (option == "full")
                {
                    Console.DevConsole.Instance._consoleRect.offsetMin = new Vector2(0, 0);
                    Console.DevConsole.Instance._consoleRect.offsetMax = new Vector2(0, 0);
                    Console.DevConsole.Instance._consoleRect.anchorMin = new Vector2(0, 0);
                    Console.DevConsole.Instance._consoleRect.anchorMax = new Vector2(1, 1);
                }
                else if (option == "half")
                {
                    Console.DevConsole.Instance._consoleRect.offsetMin = new Vector2(0, Screen.height * 0.5f);
                    Console.DevConsole.Instance._consoleRect.offsetMax = new Vector2(0, 0);
                    Console.DevConsole.Instance._consoleRect.anchorMin = new Vector2(0, 0);
                    Console.DevConsole.Instance._consoleRect.anchorMax = new Vector2(1, 1);
                }
                else if (option == "small")
                {
                    Console.DevConsole.Instance._consoleRect.offsetMin = new Vector2(0, Screen.height * 0.75f);
                    Console.DevConsole.Instance._consoleRect.offsetMax = new Vector2(0, 0);
                    Console.DevConsole.Instance._consoleRect.anchorMin = new Vector2(0, 0);
                    Console.DevConsole.Instance._consoleRect.anchorMax = new Vector2(1, 1);
                }
                else if (option == "lite")
                {
                    Console.DevConsole.Instance._consoleRect.offsetMin = new Vector2(0, Screen.height * 0.85f);
                    Console.DevConsole.Instance._consoleRect.offsetMax = new Vector2(0, 0);
                    Console.DevConsole.Instance._consoleRect.anchorMin = new Vector2(0, 0);
                    Console.DevConsole.Instance._consoleRect.anchorMax = new Vector2(1, 1);
                }
                else if (option == "minimal")
                {
                    Console.DevConsole.Instance._consoleRect.offsetMin = new Vector2(Screen.width * 0.2f, Screen.height - 40);
                    Console.DevConsole.Instance._consoleRect.offsetMax = new Vector2(Screen.width * -0.2f, 0);
                    Console.DevConsole.Instance._consoleRect.anchorMin = new Vector2(0, 0);
                    Console.DevConsole.Instance._consoleRect.anchorMax = new Vector2(1, 1);
                }
            }
            else if(mode == "debug")
            {
                if(option == "true")
                {
                    Application.logMessageReceived += DevConsole.Instance.HandleLog;
                }
                else if( option == "false")
                {
                    Application.logMessageReceived -= DevConsole.Instance.HandleLog;
                }
                else if (option == "resolution")
                {
                    AddStaticMessageToConsole("<color='#ffffff'>Screen Resolution: " + "x = " + Screen.width + " | y = " + Screen.height + "</color>");
                }
            }
            else
            {
                AddStaticMessageToConsole(TypeNotSupported);
            }
        }
        else
        {
            AddStaticMessageToConsole(ParametersAmount);
        }
    }

    public static CommandCoreConsole CreateCommand()
    {
        return new CommandCoreConsole();
    }
}
